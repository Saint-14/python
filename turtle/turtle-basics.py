import turtle
# main function
def main():
	screen = turtle.Screen()
	t = turtle.Turtle()
	#t.tracer(0, 0)

	t.penup()
	t.goto(-200,100)
	t.pendown()
	t.width(10)
	t.pencolor("#7C6522")
	t.fillcolor("#0B74A6")
	t.begin_fill()
	
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.rt(90)
	t.fd(100)
	t.end_fill()
	t.penup()
	
	t.begin_fill()
	t.pencolor("#7C6522")
	t.fillcolor("#805E16")
	t.goto(50,200)
	t.pendown()
	t.fd(200)
	t.rt(90)
	t.fd(400)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.fd(400)
	t.end_fill()
	
	
	
	
	screen.exitonclick()	
	

if __name__ == "__main__":
		main()
