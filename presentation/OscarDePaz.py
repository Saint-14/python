import turtle
# main function
def joemama():
	w = turtle.Screen()
	w.clear()
	w.bgcolor("#ff00ff")
	screen = turtle.Screen()
	t = turtle.Turtle()
	turtle.tracer(0, 0)
	
	
	t.penup()
	t.goto(-100,-100)
	t.pendown()
	t.pencolor("#000000")#black
	t.fillcolor("#ffffff")#white
	t.begin_fill()
	t.width(10)
	t.circle(200)
	t.end_fill()
	
	t.penup()
	t.goto(70,100)
	t.pendown()
	t.width(10)
	t.circle(20)
	
	t.penup()
	t.goto(-40,100)
	t.pendown()
	t.pencolor("#000000")#black
	t.width(10)
	t.circle(20)
		
	t.penup()
	t.goto(10,-20)
	t.pendown()
	t.pencolor("#000000")#black
	t.width(10)
	t.bk(80)
	
	t.penup()
	t.goto(-180,-100)
	t.pendown()
	t.pencolor("#000000")#black
	t.fillcolor("#21ff00")#green
	t.begin_fill()
	t.width(10)
	t.fd(200)
	t.rt(90)
	t.fd(500)
	t.rt(90)
	t.fd(200)
	t.rt(90)
	t.fd(500)
	t.end_fill()
	
	t.penup()
	t.goto(100,250)
	t.pendown()
	t.width(10)
	t.pencolor("#000000")
	t.lt(90)
	t.fd(400)
	
	t.penup()
	t.goto(30,250)
	t.pendown()
	t.fillcolor("#1F291D")#grey
	t.begin_fill()
	t.width(10)
	t.rt(90)
	t.fd(250)
	t.lt(90)
	t.fd(250)
	t.lt(90)
	t.fd(250)
	t.end_fill()
	
	t.penup()
	t.goto(-100,-100)
	t.pendown()
	t.pencolor("#000000")#black
	t.width(10)
	t.lt(30)
	t.fd(90)
	t.lt(120)
	t.fd(90)
	
	t.penup()
	t.goto(-55,-180)
	t.pendown()
	t.pencolor("#000000")#black
	t.fillcolor("#ff0000")#red
	t.begin_fill()
	t.width(10)
	t.rt(135)
	t.fd(150)
	t.rt(45)
	t.fd(100)
	t.rt(125)
	t.fd(100)
	t.rt(45)
	t.fd(150)
	t.penup()
	t.goto(-100,-100)
	t.pendown()
	t.rt(70)
	t.fd(100)
	t.end_fill()
	
	t.penup()
	t.goto(-40,20)
	t.pendown()
	t.fillcolor("#5f0404")#brown
	t.begin_fill()
	t.width(10)
	t.fd(60)
	t.rt(45)
	t.fd(40)
	t.rt(135)
	t.fd(120)
	t.rt(135)
	t.fd(40)
	t.end_fill()
	
	
	screen.exitonclick()	

def main():
	joemama()

if __name__ == "__main__":
		main()
#lt = left turn
#rt = right turn
#bk = back
